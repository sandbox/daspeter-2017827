<?php
/**
 * @file
 * Ctools context type plugin to provide some caching relevant information.
 */

$plugin = array(
  'title' => t('Cache Actions'),
  'description' => t('Caching Relevant Context. Faster than tokens.'),
  'context' => 'ctools_context_cache_actions_create',
  'context name' => 'cache_actions',
  'keyword' => 'cache_actions',
  // Provides a list of items which are exposed as keywords.
  'convert list' => 'ctools_cache_actions_context_convert_list',
  // Convert keywords into data.
  'convert' => 'ctools_cache_actions_context_convert',
);

/**
 * Create a context.
 */
function ctools_context_cache_actions_create($empty, $data = NULL, $conf = FALSE, $plugin = array()) {
  $context = new ctools_context('cache_actions', $data);
  $context->plugin = 'cache_actions';
  $context->title = t('Cache Actions');
  return $context;
}

/**
 * Provide a list of sub-keywords.
 *
 * Re-use the token integration without the full overhead.
 */
function ctools_cache_actions_context_convert_list() {
  $keywords = array();
  module_load_include('inc', 'cache_actions', 'cache_actions.tokens');
  $tokens = cache_actions_token_info();
  if (isset($tokens['tokens']['site'])) {
    foreach ($tokens['tokens']['site'] as $token => $token_data) {
      $keywords['site-' . $token] = $token_data['name'] . ': ' . $token_data['description'];
    }
  }
  return $keywords;
}

/**
 * Convert a context property into a string to be used as a keyword.
 */
function ctools_cache_actions_context_convert($context, $type) {
  if (strpos($type, 'site-') === 0) {
    module_load_include('inc', 'cache_actions', 'cache_actions.tokens');
    list(,$token) = explode('site-', $type);
    $tokens = cache_actions_tokens('site', array($token => $token));
    if (isset($tokens[$token])) {
      return $tokens[$token];
    }
  }
}
