<?php

/**
 * @file
 * This file contains a cache that just implements the views cache plugin.
 */

/**
 * Plugin that caches views infinitely. This is not really
 * that useful unless you want rules to invalidate your cache.
 */
class cache_actions_plugin_cache_rules extends views_plugin_cache {
  /**
   * Return a string to display as the clickable title for the
   * access control.
   */
  function summary_title() {
    return t('Rules');
  }

  /**
   * Initialize the plugin.
   *
   * @param view $view
   *   The view object.
   * @param views_plugin_display $display
   *   The display handler.
   */
  function  init(&$view, &$display) {
    parent::init($view, $display);
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['use_args_as_key'] = array('default' => FALSE, 'bool' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['use_args_as_key'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add the views arguments to the cache key'),
      '#default_value' => $this->options['use_args_as_key'],
    );
  }

  /**
   * Since rules will be triggering our cache invalidation,
   * the cache will never expire.
   *
   * @param string $type
   *   Not used in this plugin.
   *
   * @return FALSE
   *   Returns always false.
   */
  function cache_expire($type) {
    return FALSE;
  }

  /**
   * We override the default caching mechanism, since it nukes everything.
   * We are just going to flush the actual display.
   */
  function cache_flush() {
    cache_clear_all($this->view->name . ':' . $this->display->id, $this->table, TRUE);
  }

  /**
   * Returns cache key.
   *
   * @param array $key_data
   *   Additional data for cache segmentation and/or overrides for default
   *   segmentation.
   *
   * @return string
   */
  function get_cache_key($key_data = array()) {
    // If this is a search api index use the dedicated plugin to get the cache
    // key.
    if (class_exists('SearchApiViewsQuery') && is_a($this->view->query, 'SearchApiViewsQuery')) {
      $search_api_cache_plugin = new SearchApiViewsCache();
      $search_api_cache_plugin->init($this->view, $this->display);
      $search_api_cache_plugin->options = $this->options;
      $key = $search_api_cache_plugin->get_cache_key($key_data);
    }
    else {
      $key = parent::get_cache_key($key_data);
    }
    if (!empty($this->options['use_args_as_key'])) {
      $key .= (isset($this->view->args)) ? ':' . md5(serialize($this->view->args)) : '';
    }
    return $key;
  }

}
