<?php
/**
 * @file
 * Token callbacks for the cache action module.
 */

/**
 * Implements hook_token_info().
 */
function cache_actions_token_info() {
  $tokens = array();
  // Add a token with the change date of nodes as timestamp.
  if (module_exists('node')) {
    $tokens['tokens']['node']['changed-timestamp'] = array(
      'name' => t('Timestamp of the change date'),
      'description' => t('The Unix timestamp of the change date.'),
    );
  }
  if (module_exists('comment')) {
    $tokens['tokens']['comment']['changed-timestamp'] = array(
      'name' => t('Timestamp of the change date'),
      'description' => t('The Unix timestamp of the change date.'),
    );
  }
  if (module_exists('file')) {
    $tokens['tokens']['file']['changed-timestamp'] = array(
      'name' => t('Timestamp of the change date'),
      'description' => t('The Unix timestamp of the change date.'),
    );
  }
  if (module_exists('commerce_product')) {
    $tokens['tokens']['comment']['changed-timestamp'] = array(
      'name' => t('Timestamp of the change date'),
      'description' => t('The Unix timestamp of the change date.'),
    );
  }
  if (module_exists('menu')) {
    $tokens['tokens']['site'] = array_fill_keys(cache_actions_get_menu_tokens(), array(
      'name' => t('Menu: Timestamp of the last change'),
      'description' => t('The Unix timestamp of the change date.'),
    ));
  }
  return $tokens;
}

/**
 * Build the token keys from the available menus.
 *
 * @return array
 *   List with token names.
 */
function cache_actions_get_menu_tokens() {
  $menus = array_keys(menu_get_menus());
  $menus = array_combine($menus, $menus);
  return array_map(function($menu) {
    $menu .= '-changed-timestamp';
    return $menu;
  }, $menus);
}

/**
 * Implements hook_token_info_alter().
 */
function cache_actions_token_info_alter(&$tokens) {
  // Use the alter hook to ensure the token group for the entity exists.
  if (module_exists('entity_modified')) {
    $entity_info = entity_get_info();
    foreach ($entity_info as $entity_type => $info) {
      if ($entity_type == 'taxonomy_term') {
        $entity_type = 'term';
      }
      // If native modified timestamp known.
      if (isset($tokens['types'][$entity_type]) && !isset($tokens['tokens'][$entity_type]['changed-timestamp'])) {
        $tokens['tokens'][$entity_type]['changed-timestamp'] = array(
          'name' => t('Timestamp of the change date'),
          'description' => t('The Unix timestamp of the change date.'),
        );
      }
    }
  }
}

/**
 * Implements hook_tokens().
 */
function cache_actions_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  if (!empty($data['entity'])) {
    return $replacements;
  }

  $menu_tokens = cache_actions_get_menu_tokens();
  foreach ($tokens as $token => $placeholder) {
    if (($menu = array_search($token, $menu_tokens)) !== FALSE) {
      $menu_changed_timestamps = variable_get('cache_actions_menu_changed_timestamp', array());
      $replacements[$placeholder] = REQUEST_TIME + 3;
      if (isset($menu_changed_timestamps[$menu])) {
        $replacements[$placeholder] = $menu_changed_timestamps[$menu];
      }
    }
    switch ($token) {

      case 'changed-timestamp':
        $entity_type = $type;
        if (!empty($data['entity_type'])) {
          $entity_type = $data['entity_type'];
        }
        if ($entity_type == 'term') {
          $entity_type = 'taxonomy_term';
        }
        // If the entity supports revisions replace the changed timestamp
        // with revision timestamp.
        $info = entity_get_info($entity_type);
        if (!empty($info['entity keys']['revision']) && isset($data[$type]->revision_timestamp)) {
          $replacements[$placeholder] = $data[$type]->revision_timestamp;
        }
        elseif (module_exists('entity_modified')) {
          if ($item_token_type = entity_property_list_extract_type($type)) {
            $list_replacements = array();
            foreach ($data[$type] as $delta => $entity) {
              $list_replacements += array_values(cache_actions_tokens($item_token_type, array($token => $tokens[$token]), array($item_token_type => $entity), $options));
            }
            $replacements[$placeholder] = implode(':', $list_replacements);
          }
          elseif (!empty($data[$type])) {
            $replacements[$placeholder] = entity_modified_last($entity_type, $data[$type]);
          }
        }
        else {
          $properties = array(
            'node' => 'changed',
            'comment' => 'changed',
            'file' => 'timestamp',
            'commerce_product' => 'changed',
            'entityform' => 'changed',
          );
          if (isset($properties[$entity_type]) && isset($data[$type]->{$properties[$entity_type]})) {
            $replacements[$placeholder] = $data[$type]->{$properties[$entity_type]};
          }
        }
        break;

    }
  }

  return $replacements;
}
